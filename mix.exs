defmodule Kalman.MixProject do
  use Mix.Project

  def project do
    [
      app: :kalman,
      version: "1.0.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      # Docs
      name: "Kalman",
      source_url: "https://gitlab.com/smedegaard/kalman",
      docs: [
        # The main page in the docs
        # main: "readme.html",
        extras: ["readme.md"]
      ],
      package: package()
    ]
  end

  defp package() do
    [
      maintainers: ["Anders Smedegaard Pedersen"],
      contributors: ["Cees de Groot"],
      licenses: ["MIT"],
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      links: %{"GitLab" => "https://gitlab.com/smedegaard/kalman"}
    ]
  end

  defp description() do
    """
    An implementation of the Kalman Filter in Elixir.
    """
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false}
    ]
  end
end
