# Kalman

  This Module comes with no guarantee, what so ever.

> Filter raw data (often from sensors).

## What is the Kalman filter?

  In statistics and control theory, Kalman filtering, also known as linear quadratic estimation (LQE), is an algorithm that uses a series of measurements observed over time, containing statistical noise and other inaccuracies, and produces estimates of unknown variables that tend to be more accurate than those based on a single measurement alone, by estimating a joint probability distribution over the variables for each timeframe.

  https://en.wikipedia.org/wiki/Kalman_filter

## Inspiration

  This implementation is inspired by Lachlan Blackhall's Python implementation:
  https://github.com/lblackhall/pyconau2016/blob/master/kalman.py

  Watch his Pycon Australia talk for a good, easy-to-digets introduction to the Kalman filter:
  
  https://www.youtube.com/watch?v=VBfqrTOL6z0
  
## Contributors
  Version 1.0 is based on (copy/paste) of Cees de Groot implementation. Thanks for the feedback!
  https://github.com/cdegroot/palapa/tree/master/kalman

## Installation

The package can be installed by adding `kalman` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:kalman, "~> 1.0.0"}
  ]
end
```
